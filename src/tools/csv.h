#pragma once

#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <exception>
#include "../types.h"

/*
 * encapsulate writing to a csv file
 */
class Csv
{
private:
    std::ofstream file;

public:
    explicit Csv(const std::string& filename): file(filename) {};

    // writes a single number to the file
    template<typename N> void write(const N& x, const char separator = ',')
    {
        file << x << separator;
    };

    // writes a shaman number to the file, splitting result and error
    #ifdef SHAMAN
    template<typename NumberType, typename ErrorType,  typename PreciseType>
    void write(const S<NumberType,ErrorType,PreciseType>& x, const char separator = ',')
    {
        file << x.number << separator << std::abs(x.error) << separator;
    };
    #endif

    // writes a single complex number to the file
    void write(const complexNumber& x, const char separator = ',')
    {
        write(x.real());
        write(x.imag());
        file << '\t';
    };

    // write a full line, separated by the given separator, to the csv
    void write(const ComplexVector& values, const char separator = ',')
    {
        for(unsigned int i = 0; i < values.size(); i++)
        {
            write(values[i]);
        }

        file << '\n';
    };

    //---------------------------------------------------------------------------------------------
    // STATIC

    // throws an exception if a folder does not exist, otherwise return the path to the folder
    static std::string confirmedFolder(std::string folderPath)
    {
        struct stat info;
        if(stat(folderPath.c_str(), &info ) != 0)
        {
            throw std::invalid_argument("The folder '" + folderPath + "' does not exist!");
        }
        else
        {
            return folderPath;
        }
    }

    // writes a matrix to a file
    static void write(const std::string& filename, const ComplexMatrix& matrix, const char separator = ',')
    {
        std::ofstream file(filename);

        for(unsigned int r=0; r < matrix.rows(); r++)
        {
            for (unsigned int c=0; c < matrix.cols(); c++)
            {
                complexNumber x = matrix(r,c);

                #ifdef SHAMAN
                file << x.real().number << separator << std::abs(x.real().error) << separator
                     << x.imag().number << separator << std::abs(x.imag().error);
                #else
                file << x.real() << separator << x.imag();
                #endif

                if(c+1 < matrix.cols())
                {
                    file << separator;
                }
            }
            file << '\n';
        }
    }
};
