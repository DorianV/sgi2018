#ifndef PQUEUE_H
# define PQUEUE_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>

# include "array.h"

/**
 *	Structure de donnée: 'File de priorité' , ou 'Priority Queue'
 *
 *	https://fr.wikipedia.org/wiki/Tas_binaire
 */

/** typedef pratiqe pour les fonctions de comparaisons */
typedef int (*cmpf_t)(void const * leftKey, void const * rightKey);

/** sommet de la file */
typedef struct	pqueue_node_s {
	void const	* key;
	void const	* value;
	unsigned int	index; /* index dans la liste */
}		pqueue_node_t;

/** la file de priorité */
typedef struct	pqueue_s {
	array_t * nodes;
	cmpf_t	cmpf;
}		pqueue_t;

/**
 *      Macro pour iterer de manière efficace dans la file de priorité
 *      (indépendamment de l'ordre des sommets)
 *              P : une file de priorité
 *              T : le type de la variable
 *              X : le nom de la variable
 *              I : un compteur allant 'i' à 'PQ->nodes->size'
 */
# define PQUEUE_ITERATE_START(P, T, X, I)\
	ARRAY_ITERATE_START((P)->nodes, pqueue_node_t **, __pqueue_node_current_ref__, I)\
	{\
		pqueue_node_t * __pqueue_node_current__ = *__pqueue_node_current_ref__;\
		T X = (T)__pqueue_node_current__->value;
# define PQUEUE_ITERATE_STOP(P, T, X, I)\
	}\
	ARRAY_ITERATE_STOP((P)->nodes, pqueue_node_t **, __pqueue_node_current_ref__, I);

/**
 *	@require: 'cmpf': fonction comparant les clefs (voir 'strcmp()')
 *	@ensure : renvoie une nouvelle file de priorité
 *	@assign : ---------------------	
 */
pqueue_t * pqueue_new(cmpf_t cmpf);

/**
 *	@require: une file de priorité
 *	@ensure : supprime la file de la mémoire
 *	@assign : ---------------------
 */
void pqueue_delete(pqueue_t * pqueue);

/**
 *	@require: une file de priorité
 *	@ensure : renvoie 1 si la file est vide, 0 sinon
 *	@assign : ---------------------	
 */
int pqueue_is_empty(pqueue_t * pqueue);

/**
 *	@require: une file de priorité, une clef, et une valeur
 *			renvoie 1 si l'élément a pu etre inseré, 0 sinon
 *	@ensure : ajoutes l'élément dans la file.
 *			- il y a copie des adresses, pas des valeurs!
 *		  	- si la clef doit être modifié, appelé 'pqueue_decrease()'
 *	@assign : --------------------------
 */
pqueue_node_t * pqueue_insert(pqueue_t * pqueue, void const * key, void const * value);

/**
 *	@require: une file de priorité, et le sommet de la file
 *	@ensure : diminue la clef d'une valeur (<=> augmente sa priorité)
 *	@assign : ----------------------
 */
void pqueue_decrease(pqueue_t * pqueue, pqueue_node_t * node, void const * newKey);

/**
 *	@require: une file de priorité
 *	@ensure: renvoie l'élément avec la clef la plus basse (<=> la plus haute priorité)
 *	@assign: --------------------------
 */
pqueue_node_t * pqueue_minimum(pqueue_t * pqueue);

/**
 *	@require: une file de priorité
 *	@ensure: supprime et renvoie l'élément de la file avec
 *		 la clef la plus basse (<=> la plus haute priorité)
 *	@assign: --------------------------
 */
pqueue_node_t pqueue_pop(pqueue_t * pqueue);

#endif
