#pragma once

#include <stack>
#include "../types.h"
#include "point.h"

namespace Follow
{
    // goes from a point to another in a single step (as bad as it might be)
    // given the index of a target in the first point, returns the index of the target in the last point
    std::vector<unsigned int> instantStep(const Point& startPoint, const Point& endPoint, const std::vector<unsigned int>& indexTargets)
    {
        number distance; std::vector<unsigned int> indexes;
        std::tie(indexes, distance) = endPoint.findClosest(endPoint, indexTargets);

        return indexes;
    }

    // goes from a point to another with a fixed step size
    // given the index of a target in the first point, returns the index of the target in the last point
    std::vector<unsigned int> constantStep(const Point& startPoint, const Point& endPoint, const std::vector<unsigned int>& indexTargets,
                              const unsigned int maxStep = 100)
    {
        Point currentPoint = startPoint;
        std::vector<unsigned int> currentIndexes = indexTargets;
        number distance; // dummy variable, useless here

        for(unsigned int step = 0; step <= maxStep; step++)
        {
            const Point nextPoint = Point::midPoint(startPoint, maxStep-step, endPoint, step);
            std::tie(currentIndexes, distance) = nextPoint.findClosest(currentPoint, currentIndexes);
            currentPoint = nextPoint;
        }

        return currentIndexes;
    }

    // goes from a point to another with an adaptiv stepsize (trying to keep the error under a threshold)
    // given the index of a target in the first point, returns the index of the target in the last point
    // one can define minstepsize, the minimum distance between two different points, in order to avoidhaving the search stuck in local discontinuities
    // (sqrt(3) is the diagonal of a square in 3D)
    std::vector<unsigned int> adaptativStep(const Point& startPoint, const Point& endPoint, const std::vector<unsigned int>& indexTargets,
                                            const number distanceThreshold = 0.05, const number minstepsize = std::sqrt(3.)*1e-6)
    {
        Point currentPoint = startPoint;
        std::vector<unsigned int> currentIndexes = indexTargets;

        // information on potential relaxation of the threshold
        number maxAcceptedDistance = distanceThreshold;
        Point pointatMaxDistance;

        std::stack<Point> nextPoints;
        nextPoints.push(endPoint);
        while(not nextPoints.empty())
        {
            const Point& nextPoint = nextPoints.top();

            // tries to follow the target to the next point
            number distance; std::vector<unsigned int> nextIndexes;
            std::tie(nextIndexes, distance) = nextPoint.findClosest(currentPoint, currentIndexes);

            if ((distance <= distanceThreshold) || (Point::distance(currentPoint, nextPoint) <= minstepsize))
            {
                // stores information our position if we had to relax the threshold
                if (distance > maxAcceptedDistance)
                {
                    maxAcceptedDistance = distance;
                    pointatMaxDistance = currentPoint;
                }

                // no need for a refinement, we accept the point
                currentIndexes = nextIndexes;
                currentPoint = nextPoint;
                nextPoints.pop();
            }
            else
            {
                // the next step is too far away, we need to refine
                const Point intermediatePoint = Point::midPoint(currentPoint, nextPoint);
                nextPoints.push(intermediatePoint);
            }
        }

        // display informations on the worst relaxation that had to happen
        if (maxAcceptedDistance > distanceThreshold)
        {
            std::cerr << "We had to locally relax the threshold to " << maxAcceptedDistance << " around " << pointatMaxDistance << '.' << std::endl;
        }

        return currentIndexes;
    }
}
