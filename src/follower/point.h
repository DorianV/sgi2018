#pragma once

#include <cmath>
#include <limits>
#include "../types.h"
#include "../generator.h"

//-------------------------------------------------------------------------------------------------
// COSINE SIMILARITY

// returns the cosine similarity between two normalised complex vectors
number cosineSimilarity(const ComplexVector& v1, const ComplexVector& v2)
{
    return std::abs(v1.conjugate().dot(v2));
}

//-------------------------------------------------------------------------------------------------
// STEP

/*
 * used to encapsulate a point
 * contains omega, the noise, the eigenvectors and the formated eigenvalues (no need to keep the raw eigenvalues)
 */
class Point
{
public:
    unsigned int size;
    number omega;
    complexNumber xi;
    ComplexMatrix eigenVectors;
    ComplexVector formatedEigenvalues;

    // constructor that does nothing (usefull to init arrays)
    Point(): size(0), omega(0), xi(0), eigenVectors(), formatedEigenvalues()
    {}

    // creates a new point at the given omega and xi
    Point(const number omega, const unsigned int size, const complexNumber& xi = complexNumber(0.,0.)): size(size), omega(omega), xi(xi), eigenVectors(), formatedEigenvalues()
    {
        const complexNumber complexOmega = complexNumber(omega, 0.01);
        const ComplexMatrix mat = generator(complexOmega, size, xi);
        Eigen::ComplexEigenSolver<ComplexMatrix> solver(mat);
        const ComplexVector& eigenValues = solver.eigenvalues();
        eigenVectors = solver.eigenvectors();
        formatedEigenvalues = eigenValues.unaryExpr([](complexNumber e){return transformEigenvalue(e) ;});
    }

    // returns the euclidian distance between two points
    static number squareDistance(const Point& point1, const Point& point2)
    {
        const number dOmega = point1.omega - point2.omega;
        const complexNumber dxi = point1.xi - point2.xi;
        return dOmega*dOmega + std::norm(dxi);
    }

    // returns the euclidian distance between two points
    static number distance(const Point& point1, const Point& point2)
    {
        return STD::sqrt(squareDistance(point1, point2));
    }

    // returns the midpoint
    static Point midPoint(const Point& point1, const Point& point2)
    {
        const number omega = (point1.omega + point2.omega) / number(2.);
        const complexNumber xi = (point1.xi + point2.xi) / number(2.);
        unsigned int size = point1.size;

        return Point(omega, size, xi);
    }

    // returns the barycenter of two points
    static Point midPoint(const Point& point1, const number& weight1, const Point& point2, const number weight2)
    {
        const number totalWeight = weight1 + weight2;
        const number omega = (point1.omega*weight1 + point2.omega*weight2) / totalWeight;
        const complexNumber xi = (point1.xi*weight1 + point2.xi*weight2) / totalWeight;
        unsigned int size = point1.size;

        return Point(omega, size, xi);
    }

    // given several eigenvectors, return the indexes of their closest eigenvectors in this point and the maximum distance to the target
    // TODO this algorithm could be much faster with better datastructures
    std::pair<std::vector<unsigned int>, number> findClosest(const Point& targetPoint, const std::vector<unsigned int>& indexTargets) const
    {
        std::vector<unsigned int> indexclosest(indexTargets.size());
        number distance = 1.;

        // builds similarity matrix
        Matrix similarityMatrix(indexTargets.size(), eigenVectors.cols());
        for (unsigned int r=0; r<indexTargets.size(); r++)
        {
            const ComplexVector& vector = targetPoint.eigenVectors.col(indexTargets[r]);
            for (unsigned int c=0; c<eigenVectors.cols(); c++)
            {
                similarityMatrix(r,c) = cosineSimilarity(eigenVectors.col(c), vector);
            }
        }

        // finds closest vectors
        for (unsigned int v=0; v<indexTargets.size(); v++)
        {
            unsigned int bestr = 0;
            unsigned int bestc = 0;
            number bestSimilarity = 0.;

            // finds best similarity in all of the matrix
            for (unsigned int r=0; r < indexTargets.size(); r++)
            {
                for (unsigned int c=0; c < eigenVectors.cols(); c++)
                {
                    const number similarity = similarityMatrix(r,c);
                    if ((similarity > bestSimilarity) and not STD::isnan(similarity))
                    {
                        bestr = r;
                        bestc = c;
                        bestSimilarity = similarity;
                    }
                }
            }

            // stores best result so far
            indexclosest[bestr] = bestc;
            distance = 1. - bestSimilarity;
            // make given row and column unatractiv for latter iterations
            similarityMatrix.row(bestr).array() = -1.;
            similarityMatrix.col(bestc).array() = -1.;
        }

        return std::make_pair(indexclosest, distance);
    }

    // given an eigenvalue, return the index of the closest eigenvalue in this point and its distance to the target
    // TODO this algorithm could be much faster with better datastructures
    std::pair<std::vector<unsigned int>, number> findClosest(const std::vector<complexNumber>& formatedTargetEigenValues) const
    {
        std::vector<unsigned int> indexclosest(formatedTargetEigenValues.size());
        number finalDistance = 0.;

        // builds similarity matrix
        Matrix distanceMatrix(formatedTargetEigenValues.size(), eigenVectors.cols());
        for (unsigned int r=0; r<formatedTargetEigenValues.size(); r++)
        {
            const complexNumber& formatedEigenvalue = formatedTargetEigenValues[r];
            for (unsigned int c=0; c < eigenVectors.cols(); c++)
            {
                distanceMatrix(r,c) = std::abs(formatedEigenvalues[c] - formatedEigenvalue);
            }
        }

        // finds closest vectors
        for (unsigned int v=0; v<formatedTargetEigenValues.size(); v++)
        {
            unsigned int bestr = 0;
            unsigned int bestc = 0;
            number minDistance = std::numeric_limits<number>::infinity();

            // finds best similarity in all of the matrix
            for (unsigned int r=0; r<formatedTargetEigenValues.size(); r++)
            {
                for (unsigned int c=0; c<eigenVectors.cols(); c++)
                {
                    const number distance = distanceMatrix(r,c);
                    if (distance < minDistance)
                    {
                        bestr = r;
                        bestc = c;
                        minDistance = distance;
                    }
                }
            }

            // stores best result so far
            indexclosest[bestr] = bestc;
            finalDistance = minDistance;
            // make given row and column unatractiv for latter iterations
            distanceMatrix.row(bestr).array() = std::numeric_limits<number>::infinity();
            distanceMatrix.col(bestc).array() = std::numeric_limits<number>::infinity();
        }

        return std::make_pair(indexclosest, finalDistance);
    }
};

// streaming operator to properly display a point
std::ostream& operator<< (std::ostream& stream, const Point& point)
{
    stream << "{omega: " << point.omega << " xi: " << point.xi << '}';
    return stream;
}

//-------------------------------------------------------------------------------------------------
