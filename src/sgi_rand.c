/**
 * The implementation is a Knuth & Lewis RNG
 */

# include <float.h>
# include <math.h>
# ifndef M_PI
#  define M_PI 3.14159265358979323846
# endif
# include <stdlib.h>

# include "sgi_rand.h"

__thread unsigned int x = 0;

void sgi_srand(unsigned int seed) {
	x = seed;
}

unsigned int sgi_rand(void) {
	return (x = 1664525 * x + 1013904223);
}

int sgi_rand_i(void) {
	return (int)sgi_rand();
}

unsigned int sgi_rand_u(void) {
	return sgi_rand();
}

float sgi_rand_f(void) {
	return (float)(sgi_rand() / (float)SGI_MAX_RAND);
}

double sgi_rand_d(void) {
	return (double)(sgi_rand() / (double)SGI_MAX_RAND);
}

double sgi_rand_uniform(double a, double b) {
	return a + sgi_rand_d() * b;
}

/* TODO : optimize me */
double sgi_rand_gauss(double mu, double sigma) {
	static int done = 0;
	static double y1;
	if (done) {
		done = 0;
		return y1 * sigma + mu;
	}
	done = 1;
	
	double u1, u2;
	do {
		u1 = sgi_rand_uniform(0.0, 1.0);
		u2 = sgi_rand_uniform(0.0, 1.0);
	} while (u1 <= DBL_EPSILON);
	
	double y0 = sqrt(-2.0 * log(u1)) * cos(2.0 * M_PI * u2);
	y1 = sqrt(-2.0 * log(u1)) * sin(2.0 * M_PI * u2);
	return y0 * sigma + mu;
}

double * sgi_rand_vec(unsigned int n, double * dst) {
	if (dst == NULL) {
		dst = (double *) malloc(sizeof(double) * n);
		if (dst == NULL) {
			return NULL;
		}
	}
	unsigned int i;
	for (i = 0 ; i < n ; i++) {
		dst[i] = sgi_rand_d();
	}
	return dst;
}

